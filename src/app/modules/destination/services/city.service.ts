import { Injectable } from '@angular/core';
import { delay, Observable, of } from 'rxjs';
import { ICity } from '../models/city.models';

import * as City from '../../../shared/data/city.json';

@Injectable({
  providedIn: 'root',
})
export class CityService {
  constructor() {}

  fetchCities(): Observable<ICity[]> {
    return of(City).pipe(delay(1000));
  }
}
