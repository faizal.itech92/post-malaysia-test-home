export interface ICity {
  name: string;
  count: number;
  content: string;
}
