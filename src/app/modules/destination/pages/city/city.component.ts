import { Component, OnInit } from '@angular/core';
import { tap } from 'rxjs';
import { ICity } from '../../models/city.models';
import { CityService } from '../../services/city.service';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css'],
})
export class CityComponent implements OnInit {
  cities: ICity[] = [];
  contents: string = '';
  columns = [1, 2, 3];

  constructor(private _cityService: CityService) {}

  ngOnInit(): void {
    this.getCities();
  }

  getCities() {
    this._cityService
      .fetchCities()
      .pipe(tap((data: any) => (this.cities = this.sortArray(data.default))))
      .subscribe();
  }

  checkValid(column: number, cityIndex: number) {
    if (column == 1 && cityIndex < 4) return true;
    if (column == 2 && cityIndex >= 4 && cityIndex < 8) return true;
    if (column == 3 && cityIndex >= 8) return true;
    return false;
  }

  private sortArray(arr: ICity[]): ICity[] {
    return arr.sort(function (a: ICity, b: ICity) {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    });
  }
}
