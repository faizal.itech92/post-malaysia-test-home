import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DestinationRoutingModule } from './destination-routing.module';
import { CityComponent } from './pages/city/city.component';

@NgModule({
  imports: [CommonModule, DestinationRoutingModule],
  declarations: [CityComponent],
})
export class DestinationModule {}
