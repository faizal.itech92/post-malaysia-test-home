import {
  Directive,
  Input,
  OnInit,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';

@Directive({
  selector: '[strictLoop]',
})
export class StrictLoopDirective implements OnInit {
  configuration: any;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) {}

  ngOnInit() {
    this.updateView();
  }

  @Input()
  set strictLoop(val: any) {
    this.configuration = val;
    this.updateView();
  }

  private updateView() {
    if (this.checkValid()) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }

  private checkValid() {
    if (this.configuration.column == 1 && this.configuration.cityIndex < 4)
      return true;
    if (
      this.configuration.column == 2 &&
      this.configuration.cityIndex >= 4 &&
      this.configuration.cityIndex < 8
    )
      return true;
    if (this.configuration.column == 3 && this.configuration.cityIndex >= 8)
      return true;
    return false;
  }
}
